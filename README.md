# Edge++

A simple engine that allows programming your website with C++ just by doing cin and cout statements.

## Usage

See main-project

## Building

The project uses `parcel-bundler` for its build system. Once `parcel-bundler` is installed into your system, you must call
`parcel index.html` on your project directory and place the qjs.wasm binary that you want to use inside the `dist/` folder that will be
created and inside a `static/` folder.

Many different tools can be used to build .wasm binaries, in our examples we used `wasienv` and the command 
```bash
wasic++ main.cpp -o qjs
```

## Reminder

Remember to create all folders that are accessed before using the website (sites, static, fitxerCompilar...)
