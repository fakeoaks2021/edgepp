# Authors

The authors of this project are the FakeOaks team: Rubén Aciego, Jofre Costa, Sergi Soler i Dani Vilardell. We also want to give credit to the following pages and author as we have used their code and ideas to run our project:

 - Some files of the [wasmer-js project](https://github.com/wasmerio/wasmer-js/blob/master/packages/wasm-terminal/src/process/process.ts).

 - [Some fragments](https://getmdl.io/) for building our webpage.

 - Google for [Google Code Prettify](https://github.com/googlearchive/code-prettify).
