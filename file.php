<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://code.getmdl.io/1.3.0/material.indigo-blue.min.css" rel="stylesheet">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="styles.css" rel="stylesheet">
    <style>#file {
            display: none
        }</style>
    <title>Edge++</title>
</head>
<body>
<style>

    .demo-layout-transparent .mdl-layout__header,
    .demo-layout-transparent .mdl-layout__drawer-button {
        /* This background is dark, so we set text to white. Use 87% black instead if
           your background is light. */
        color: white;
    }
</style>
<div id="page-container">
    <div id="content-wrap">
        <!-- all other page content -->
        <!-- Simple header with fixed tabs. -->
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header
            mdl-layout--fixed-tabs">
            <!-- Uses a header that scrolls with the text, rather than staying
           locked at the top -->
            <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
            <link href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css" rel="stylesheet">
            <!-- Material Design icon font -->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title" style="font-size: 25px">Edge++</span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation -->
                    <nav class="mdl-navigation">
                        <a class="mdl-navigation__link" href="index.html">MAIN</a>
                        <a class="mdl-navigation__link" href="example.html">EXAMPLES</a>
                        <a class="mdl-navigation__link" href="documentation.html">DOCUMENTATION</a>
                        <a class="mdl-navigation__link" href="https://gitlab.com/fakeoaks2021/edgepp">GITLAB</a>
                    </nav>
            </header>


            </div>
            <br><br><br><br><br><br>
            <div style="text-align:justify; margin-left:2em">

            <?php
            require_once "wordlist.php";
            $uploaddir = './fitxerCompilar/';
            $cppfile = $uploaddir . 'code.cc';
            $htmlfile = $uploaddir . 'index.html';


            echo '<pre>';
            if (move_uploaded_file($_FILES['cppfile']['tmp_name'], $cppfile)) {
                echo "File is valid, and was successfully uploaded.\n";
            }

            if (move_uploaded_file($_FILES['htmlfile']['tmp_name'], $htmlfile)) {
                echo "File is valid, and was successfully uploaded.\n";
            }
            echo exec("rm dist/*");
            //echo 'Here is some more debugging info:\n';
            //print_r($_FILES);
            echo exec("rm ./static/qjs.wasm");
            echo exec('wasic++ '. $cppfile. ' -O3 -o ./static/qjs.wasm -fno-exceptions');

            $id = "";

            for ($i = 0; $i < 3; $i++) {
                $word = $words[rand(0,7776)];
                $id = $id . $word . '-';
            }

            $word = $words[rand(0,7776)];
            $id = $id . $word;

            $path = "sites/" . $id;
            echo shell_exec("sed '/^\s*<head>*/a <script type=\"text/javascript\" src=\"main.js\"></script>' " . $htmlfile . " > doremi ");
            echo exec("rm js-side/index.html");
            echo exec("mv doremi ./js-side/index.html");
            echo shell_exec("parcel build ./js-side/index.html --public-url /" . $path);

            echo exec("cp ./static/qjs.wasm dist/qjs.wasm" );
            echo exec("mv dist " . $path ."");

            echo ('<br><form action="' . $path . '">
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Link to the page</button>
            </form>');

            print "</pre>";

            ?>
                
        </div>
    </div>
</div>

<div id="footer">
    <footer class="mdl-mini-footer">
        <div class="mdl-mini-footer__left-section">
            <div class="mdl-logo" style="font-size: 20px">Edge++</div>
            <ul class="mdl-mini-footer__link-list">
                <li><a href="https://www.youtube.com/watch?v=2Q_ZzBGPdqE" style="font-size: 20px">Help</a></li>
                <li><a href="http://nyan.cat" style="font-size: 20px">Nyan cat</a></li>
            </ul>
        </div>
    </footer>
</div>
</body>

</html>
